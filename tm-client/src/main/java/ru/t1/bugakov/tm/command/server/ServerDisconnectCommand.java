package ru.t1.bugakov.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.command.AbstractCommand;
import ru.t1.bugakov.tm.enumerated.Role;

public final class ServerDisconnectCommand extends AbstractCommand {

    @Override
    public void execute() {
        getServiceLocator().getAuthEndpoint().disconnect();
    }

    @Override
    public @NotNull
    String getName() {
        return "server-disconnect";
    }

    @Override
    public @Nullable
    String getArgument() {
        return null;
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Disconnect from server";
    }

    @Override
    public @NotNull
    Role[] getRoles() {
        return null;
    }
}
