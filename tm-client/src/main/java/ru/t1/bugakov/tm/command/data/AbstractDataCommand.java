package ru.t1.bugakov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.bugakov.tm.command.AbstractCommand;
import ru.t1.bugakov.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected IDomainEndpoint getDomainEndpoint() {
        return getServiceLocator().getDomainEndpoint();
    }

    @Override
    public @Nullable
    String getArgument() {
        return null;
    }

    @Override
    public @NotNull
    Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
