package ru.t1.bugakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.bugakov.tm.api.endpoint.IUserEndpoint;
import ru.t1.bugakov.tm.command.AbstractCommand;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserEndpoint getUserEndpoint() {
        return serviceLocator.getUserEndpoint();
    }

    @NotNull
    protected IAuthEndpoint getAuthEndpoint() {
        return serviceLocator.getAuthEndpoint();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    public void showUser(@NotNull final User user) {
        System.out.println("ID:" + user.getId());
        System.out.println("LOGIN:" + user.getLogin());
    }

}
