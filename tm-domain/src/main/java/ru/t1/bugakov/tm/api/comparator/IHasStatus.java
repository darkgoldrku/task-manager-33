package ru.t1.bugakov.tm.api.comparator;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull final Status status);

}
