package ru.t1.bugakov.tm.dto.request.task;

import lombok.NoArgsConstructor;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class TaskClearRequest extends AbstractUserRequest {
}
