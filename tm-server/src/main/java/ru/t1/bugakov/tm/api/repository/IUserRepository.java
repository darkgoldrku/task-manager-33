package ru.t1.bugakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.model.User;

public interface IUserRepository extends IAbstractRepository<User> {

    @Nullable
    User findByLogin(@NotNull final String login);

    @Nullable
    User findByEmail(@NotNull final String email);

    @NotNull
    Boolean isLoginExist(@NotNull final String login);

    @NotNull
    Boolean isEmailExist(@NotNull final String email);

    @Nullable
    User lockUserByLogin(@NotNull final String login);

    @Nullable
    User unlockUserByLogin(@NotNull final String login);

}
