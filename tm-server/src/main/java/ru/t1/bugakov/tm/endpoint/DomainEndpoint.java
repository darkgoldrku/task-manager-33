package ru.t1.bugakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.bugakov.tm.api.service.IDomainService;
import ru.t1.bugakov.tm.api.service.IServiceLocator;
import ru.t1.bugakov.tm.dto.request.data.*;
import ru.t1.bugakov.tm.dto.response.data.*;
import ru.t1.bugakov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.bugakov.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IDomainService getDomainService() {
        return getServiceLocator().getDomainService();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupLoadResponse BackupLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataBackupLoad();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupSaveResponse BackupSave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataBackupSave();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64LoadResponse Base64Load(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64LoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataBase64Load();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64SaveResponse Base64Save(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64SaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataBase64Save();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinaryLoadResponse BinaryLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinaryLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataBinaryLoad();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinarySaveResponse BinarySave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinarySaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataBinarySave();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadFasterXmlResponse JsonLoadFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataJsonLoadFasterXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadJaxBResponse JsonLoadJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataJsonLoadJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveFasterXmlResponse JsonSaveFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataJsonSaveFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveJaxBResponse JsonSaveJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataJsonSaveJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlLoadFasterXmlResponse XmlLoadFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataXmlLoadFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlLoadJaxBResponse XmlLoadJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataXmlLoadJaxB();
        return new DataXmlLoadJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlSaveFasterXmlResponse XmlSaveFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataXmlSaveFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlSaveJaxBResponse XmlSaveJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataXmlSaveJaxB();
        return new DataXmlSaveJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlLoadFasterXmlResponse YamlLoadFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataYamlLoadFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlSaveFasterXmlResponse YamlSaveFasterXml(
            @NotNull final DataYamlSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataYamlSaveFasterXml();
        return new DataYamlSaveFasterXmlResponse();
    }

}
