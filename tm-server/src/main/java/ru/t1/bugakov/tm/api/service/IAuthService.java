package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@Nullable final String login, @Nullable final String password, @Nullable final String email);

    @NotNull
    User check(@Nullable String login, @Nullable String password);
}
